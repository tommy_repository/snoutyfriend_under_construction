const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

const path = require("path");
app.use('/', express.static(path.join(__dirname, '/../src')));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
