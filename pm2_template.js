module.exports = {
  apps : [{
    name: "snoutyfriend_under_construction",
    script: "node server/server.js",
    env: {
      NODE_ENV: "development",
      PORT: 3000,
    }
  }]
};